/*
FileName : config.constants.js
Date : 15th March 2018
Description : This file consist of list of contants to use in the APIs
*/

module.exports = {
    'dev': 'mongodb://127.0.0.1:27017/demo_database_dev'
};