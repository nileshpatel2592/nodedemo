/*
FileName : dbConnection.js
Date : 17th Mar 2018
Description : This file consist of code for MongoDB connection
*/

var config = require('./config.constants');
var mongoose = require('mongoose');

// database connection
let databaseName = process.env.NODE_ENV;
mongoose.connect(config[databaseName]);
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log('Database is successfully connected');
});