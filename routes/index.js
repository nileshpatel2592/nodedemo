var express = require('express');
var router = express.Router();

// connect database
require('./../config/dbConnection');

// require controller
const userController = require('./../controller/userController');

/* GET home page. */
router.get('/page', function (req, res, next) {
  res.render('index', { title: 'Node.js Demo',fname: 'Neel', lname: 'Patel', email: 'neel@gmail.com', mobile: '9624007066'});
});

router.post('/user', userController.addUser);

module.exports = router;
