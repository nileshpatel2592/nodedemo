const userModel = require('./../model/userModel');

module.exports.addUser = async (req, res) => {
    if (!req.body.email || !req.body.mobile || !req.body.firstName || !req.body.lastName || !req.body.password) {
        return res.status(400).json({ success: false, message: 'Given parameters are invalid' });
    }
    try {
        const user = new userModel({
            email: req.body.email,
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            mobile: req.body.mobile,
            password: req.body.password
        })
        await user.save();
        return res.status(200).json({ success: true, message: "User created" });
    } catch (error) {
        if (error.name === 'MongoError' && error.code === 11000) {
            return res.status(409).json({ success: false, message: error.message });
        }
        return res.status(500).json({ success: false, message: error });
    }

}